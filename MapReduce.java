import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.fs.FileSystem;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
public class MapReduce
{

	public static void main(String[] args) throws Exception
	{

		Configuration conf = new Configuration();
		conf.set("file.pattern", ".*(Users\\.xml)");
		Job job = Job.getInstance(conf, "AccountId");
		job.setJarByClass(MapReduce.class);
		job.setMapperClass(TaskOneMapper.class);
		job.setReducerClass(AccountIdReducer.class);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileInputFormat.setInputPathFilter(job, RegexPathFilter.class);
		FileInputFormat.setInputDirRecursive(job, true);
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.waitForCompletion(true);
	}

	public static class TaskOneMapper extends Mapper<Object, Text,IntWritable,IntWritable>
	{
			public void map(Object key, Text value, Context context) throws IOException, InterruptedException
			{
					Pattern pattern = Pattern.compile("AccountId=\"([^\"]*)\"");
					Matcher matcher =  pattern.matcher(value.toString());
					Pattern pattern2 = Pattern.compile("Age=\"([^\"]*)\"");
					Matcher matcher2 =  pattern2.matcher(value.toString());
					if(matcher.find() && matcher2.find())
					{
							context.write(new IntWritable(Integer.parseInt(matcher.group(1))),new IntWritable(Integer.parseInt(matcher2.group(1))));
					}

					/*IntWritable result = new IntWritable();
					if (matcher.find()==true)
					{
						result.set(Integer.parseInt(matcher.group(1)));
						context.write(result, NullWritable.get());
					}*/


			}
	}

	public static class AccountIdReducer extends Reducer<IntWritable, IntWritable, IntWritable, IntWritable>
	{
				public void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException
						{
							Iterator i = values.iterator();
							context.write(key, (IntWritable)(i.next()));

        		}
    }



	public static class RegexPathFilter extends Configured implements PathFilter
	{


	    Pattern pattern;
	    Configuration conf;
	    FileSystem fs;

	    @Override
	    public boolean accept(Path path) {
	        try {
	            if (fs.isDirectory(path)) {
	                return true;
	            } else {
	                Matcher m = pattern.matcher(path.toString());
	                System.out.println("Is path: " + path.toString() + " matches "
	                        + conf.get("file.pattern") + " ? , " + m.matches());
	                return m.matches();
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	            return false;
	        }
	    }

	    @Override
	    public void setConf(Configuration conf) {
	        this.conf = conf;
	        if (conf != null) {
	            try {
	                fs = FileSystem.get(conf);
	                if(conf.get("file.pattern") == null) {
	                    conf.set("file.pattern", ".*");
	                }
	                pattern = Pattern.compile(conf.get("file.pattern"));
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}

}
