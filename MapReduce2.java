
import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.fs.FileSystem;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;

public class MapReduce2 {

	public static void main(String[] args) throws Exception {

		Configuration conf = new Configuration();
		conf.set("file.pattern", ".*(Posts\\.xml)");
		Job job = Job.getInstance(conf, "Tags");
		job.setJarByClass(MapReduce2.class);
		job.setMapperClass(TaskTwoMapper.class);
		job.setReducerClass(PostReducer.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileInputFormat.setInputPathFilter(job, RegexPathFilter.class);
		FileInputFormat.setInputDirRecursive(job, true);
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.waitForCompletion(true);
	}

	public static class TaskTwoMapper extends Mapper<Object, Text, Text, IntWritable> {
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			
			Path fileSplit = ((FileSplit) context.getInputSplit()).getPath();
			String filename = (String)((FileSplit) context.getInputSplit()).getPath().toString();
			Pattern pattern = Pattern.compile("Tags=\"([^\"]*)\"");
			Matcher matcher = pattern.matcher(value.toString());
			
			Pattern pattern2 = Pattern.compile("&lt;(.*?)&gt;");
			Matcher matcher2 = null;
			int numGroup = 0;
			if (matcher.find()) {
				matcher2 = pattern2.matcher(matcher.group(1));
				numGroup = matcher2.groupCount();
				// System.out.println(matcher.group(1));
				// System.out.println(matcher2.groupCount());
			}
			if (numGroup > 0) {


				Pattern patternF = Pattern.compile("stackexchange/(.*?)/Posts.xml");
				Matcher matcherF = patternF.matcher(filename);
				matcherF.find();
				while (matcher2.find()) {
					context.write(new Text(matcher2.group(1)+","+matcherF.group(1)+","), new IntWritable(1));
				}
			}

			/*
			 * IntWritable result = new IntWritable(); if (matcher.find()==true)
			 * { result.set(Integer.parseInt(matcher.group(1)));
			 * context.write(result, NullWritable.get()); }
			 */

		}
	}

	public static class PostReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		private IntWritable result = new IntWritable();

		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);

			context.write(key, result);

		}
	}

	public static class RegexPathFilter extends Configured implements PathFilter {

		Pattern pattern;
		Configuration conf;
		FileSystem fs;

		@Override
		public boolean accept(Path path) {
			try {
				if (fs.isDirectory(path)) {
					return true;
				} else {
					Matcher m = pattern.matcher(path.toString());
					System.out.println("Is path: " + path.toString() + " matches " + conf.get("file.pattern") + " ? , "
							+ m.matches());
					return m.matches();
				}
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		public void setConf(Configuration conf) {
			this.conf = conf;
			if (conf != null) {
				try {
					fs = FileSystem.get(conf);
					if (conf.get("file.pattern") == null) {
						conf.set("file.pattern", ".*");
					}
					pattern = Pattern.compile(conf.get("file.pattern"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
